package com.example.weatherapplication

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Part
import retrofit2.http.Query

interface WeatherService {
    @GET("/data/{api_version}/weather")
    fun getWeather(
        @Part("api_version") version: String ="2.5",
        @Query("q") city:String,
        @Query ("appid") appId:String="fc9ebac5ce7a64f28341ee49096508c0"
        ): Call<WeatherResponse>

    companion object{
        val instace: WeatherService by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }
}