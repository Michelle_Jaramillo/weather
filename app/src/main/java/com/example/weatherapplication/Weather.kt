package com.example.weatherapplication

import com.google.gson.annotations.SerializedName

//para la serialización usa un data
data class WeatherResponse(var weather: List<Weather>?){}
data class Weather (
    @SerializedName("main")
    //el SerializedName (main) permite que pueda poner otros nombres a las variables, caso contrario deberian llevar el mismo nombre que tienene en el archivo json
    //hace que estas varisbles compaginen con las de json
    var name :  String?,
    var description:String?,
    var icon:String?){}